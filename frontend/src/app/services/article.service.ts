import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../model/article.model';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  
  public articles: Article[]=[];
  public article?: Article;

  constructor() { }

  apiUrl: String = 'http://localhost:8080/articles';

  public async fetchArticles() {
    this.articles = await this.getArticles();
  }


  public async getArticles() {
    const response = await axios.get<Article[]>("http://localhost:8080/articles");
    try {
      return response.data;
    } catch (error) {
      console.log(error);
    }
    return []
  }

  public fetchArticlesByTitle(title: string) {
    axios.get(`${this.apiUrl}`, {
      params: {
        title: title
      }
    })
      .then((res) => {
        console.log(res.data);
        this.articles = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }


  public createArticle(article: Article) {
    return axios.post(`${this.apiUrl}`, {
      id: article.id,
      title: article.title,
      author: article.author,
      summary: article.summary,
      content: article.content
    })
      .then(function (response) {
        console.log(response.data);
        return response.data
      })
      .catch(function (error) {
        console.log(error);
        return error;
      });
  }
  
  public deleteArticle(id: number) {
    axios.delete(`${this.apiUrl}/${id}`)
      .then((res) => {
        //this.article = res.data;
        this.fetchArticles();
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public getArticleDetail(id: number) {
    axios.get(`${this.apiUrl}/${id}`)
      .then((res) => {
        console.log(res.data);
        this.article = res.data;
      })
      .catch((err => {
        console.log(err);
      }))
  }

  public editArticle(id: number, title: String, summary: String, content: String) {
    axios.put(`${this.apiUrl}/${id}`, {
      title: title,
      summary: summary,
      content: content
    })
      .catch((err) => {
        console.log(err);
      })


  }






}
