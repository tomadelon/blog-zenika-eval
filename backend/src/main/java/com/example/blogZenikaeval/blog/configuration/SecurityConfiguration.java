package com.example.blogZenikaeval.blog.configuration;

import com.example.blogZenikaeval.blog.domain.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SecurityConfiguration {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable() // CSRF protection is enabled by default in the Java configuration.
                .authorizeRequests(authorize -> authorize
                        .antMatchers(HttpMethod.GET, "/articles").permitAll()
                        .antMatchers(HttpMethod.GET, "/articles/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/users").permitAll()
                        .antMatchers(HttpMethod.GET, "/users/*").permitAll()
                        .anyRequest().authenticated()
                )
                // Activate httpBasic Authentication https://en.wikipedia.org/wiki/Basic_access_authentication
                .httpBasic()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .cors()
                .and()
                .build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:4200").allowedMethods("HEAD",
                        "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

   /**
    public UserDetailsService users() {
        UserDetails thomas = User.builder()
                .username("thomas")
                .password("{noop}thomas")
                .roles("USER")
                .build();
        UserDetails alexandre = User.builder()
                .username("alex")
                .password("{noop}lama")
                .roles("USER")
                .build();
        UserDetails maud = User.builder()
                .username("maud")
                .password("{noop}maud")
                .roles("USER")
                .build();
        UserDetails amandine = User.builder()
                .username("amandine")
                .password("{noop}comete")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(thomas, alexandre, maud, amandine);
    }*/
   @Bean
   public PasswordEncoder encoder() {
       return new BCryptPasswordEncoder();
   }

   @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
       return new UserDetailsService() {
           @Override
           public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
               return userRepository.findByUsername(username).orElse(null);
           }
       };
   }
}
