package com.example.blogZenikaeval.blog;

import com.example.blogZenikaeval.blog.domain.model.article.BadLengthSummaryException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = BadLengthSummaryException.class)
    public final ResponseEntity<ApiError> badLengthSummaryException(BadLengthSummaryException e) {
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le résumé ne doit pas dépasser 50 mots"), HttpStatus.BAD_REQUEST);
    }
}
