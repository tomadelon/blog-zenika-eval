import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  
public username: string = "";
public password: string = "";


  constructor(private route: Router, private authentService:AuthenticationService) { }

  onSubmitLoggin(){
   console.log("submit");
    this.authentService.login({ username: this.username, password: this.password});
    this.authentService.verify()
    .then(() => this.route.navigate(["/"]))
    .catch(() => this.authentService.logout())
    

  }

  ngOnInit(): void {
    

  }

}
