package com.example.blogZenikaeval.blog.application;

import com.example.blogZenikaeval.blog.domain.model.users.User;
import com.example.blogZenikaeval.blog.domain.repository.UserRepository;
import com.example.blogZenikaeval.blog.web.users.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserManager {

    private final UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserManager(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User createUser(String username, String email, String password){

        User user = new User(UUID.randomUUID().toString(), username, email, password);
        user.setPassword(passwordEncoder.encode(password));

        return userRepository.save(user);
    }

    public void deleteUser(String userTid) {
        userRepository.deleteById(userTid);
    }


}
