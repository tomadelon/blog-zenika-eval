package com.example.blogZenikaeval.blog.domain.model.article;

import javax.persistence.*;


    @Entity
    @Table(name="articles")
    @Access(AccessType.FIELD)
    public class Article {
        @Id
        private String id;
        private String title;
        private String author;
        private String summary;
        private String content;


        protected Article(){
        //FOR JPA
        }

        public Article(String id, String title, String author, String summary, String content) {
            this.id = id;
            this.title = title;
            this.author = author;
            this.summary = summary;
            this.content = content;
        }

        public String getId() {
            return this.id;
        }

        public String getTitle() {
            return this.title;
        }

        public String getSummary() {
            return this.summary;
        }

        public String getContent() {
            return this.content;
        }

        public String getAuthor() {
            return author;
        }

        public void setTitle(String title) {
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }
