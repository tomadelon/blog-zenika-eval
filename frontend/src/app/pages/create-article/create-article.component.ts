import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from 'src/app/model/article.model';
import { ArticleService } from 'src/app/services/article.service';



@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss']
})
export class CreateArticleComponent implements OnInit {
  article: Article = {
    id:"",
    title: "",
    author: "",
    summary: "",
    content: ""
  }

  constructor(public articleService: ArticleService, private router: Router) { }

  async onSubmitCreateArticle() {
    await this.articleService.createArticle(this.article);
    this.router.navigate(['/']);
  }


  ngOnInit(): void {
  }


}
