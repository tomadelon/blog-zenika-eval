package com.example.blogZenikaeval.blog.domain.model.users;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;

@Entity
@Table(name = "users")
@Access(AccessType.FIELD)
public class User implements UserDetails {

    @Id
    private String tid;
    private String email;
    private String username;
    private String password;

    protected User() {
        // For JPA
    }

    public User(String tid, String email, String username, String password) {
        this.tid = tid;
        this.username = username;
        this.email = email;
        this.password= password;
    }

    public String getTid() {
        return tid;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    /**public void setUsername(String username) {
        if(!StringUtils.hasText(username)) {
            throw new IllegalArgumentException("Username can't be null");
        }
        this.username = username;
    }*/

    public String getEmail() {
        return email;
    }

    /**public void setEmail(String email) {
        if(!StringUtils.hasText(email)) {
            throw new IllegalArgumentException("Email can't be null");
        }
        this.email = email;
    }*/

    public void setPassword(String password) {
        this.password = password;
    }
}
