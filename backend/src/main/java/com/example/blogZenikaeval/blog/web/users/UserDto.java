package com.example.blogZenikaeval.blog.web.users;

public record UserDto(String username,String email, String password) {
}
