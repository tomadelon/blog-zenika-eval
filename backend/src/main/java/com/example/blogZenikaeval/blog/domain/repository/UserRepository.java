package com.example.blogZenikaeval.blog.domain.repository;


import com.example.blogZenikaeval.blog.domain.model.users.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    //public void save(User u);

    public Optional<User> findByUsername(String username) ;



}

