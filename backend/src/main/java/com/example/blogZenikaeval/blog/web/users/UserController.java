package com.example.blogZenikaeval.blog.web.users;

import com.example.blogZenikaeval.blog.application.UserManager;
import com.example.blogZenikaeval.blog.domain.model.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserManager userManager;

    @Autowired
    public UserController(UserManager userManager) {
        this.userManager = userManager;
    }

    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping("")
    @ResponseStatus(value = HttpStatus.CREATED)
    ResponseEntity<User> createUser(@RequestBody UserDto body) {
        User createdUser = userManager.createUser(body.username(),body.email(), body.password() );
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @DeleteMapping("/{userTid}")
    @ResponseStatus(HttpStatus.OK)
    void deleteUser(@PathVariable String userTid) {
        userManager.deleteUser(userTid);
    }
}



