import { Component, OnInit } from '@angular/core';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public search: string = '';

  constructor(public authentService: AuthenticationService, private articleService: ArticleService) { }

  onKeyupSearch(): void {
    this.articleService.fetchArticlesByTitle(this.search);
  }

  ngOnInit(): void {
  }

}
