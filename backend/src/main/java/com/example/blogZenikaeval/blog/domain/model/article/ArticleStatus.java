package com.example.blogZenikaeval.blog.domain.model.article;

public enum ArticleStatus {

    DRAFT,
    PUBLISHED
}
