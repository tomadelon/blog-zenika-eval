package com.example.blogZenikaeval.blog.application;

import com.example.blogZenikaeval.blog.domain.model.article.Article;
import com.example.blogZenikaeval.blog.domain.model.article.BadLengthSummaryException;
import com.example.blogZenikaeval.blog.domain.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class ArticleService {

    private final ArticleRepository articleRepository;

    @Autowired
    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    public Article createArticle(String title, String author, String summary, String
            content) throws BadLengthSummaryException {
        String[] words = summary.split(" ");
        int numberOfWords = words.length;
        if (numberOfWords <= 50 ) {
            Article newArticle = new Article(UUID.randomUUID().toString(),
                    title, author, summary, content);
            articleRepository.save(newArticle);
            return newArticle;
        }else{
            throw new BadLengthSummaryException();
        }
    }

    public Article updateArticle(String articleId, String title, String summary, String content) {
        Article articleToUpdate = articleRepository.findById(articleId).orElseThrow();
        articleToUpdate.setTitle(title);
        articleToUpdate.setSummary(summary);
        articleToUpdate.setContent(content);
        articleRepository.save(articleToUpdate);
        return articleToUpdate;
    }

    public void deleteArticle(String articleId) {
        articleRepository.deleteById(articleId);
    }

    public Iterable<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    public Optional<Article> getArticle(String articleId) {
        return articleRepository.findById(articleId);
    }

    public Iterable <Article> getAllArticleByTitle(String title) {return articleRepository.findByTitle(title);}
}



