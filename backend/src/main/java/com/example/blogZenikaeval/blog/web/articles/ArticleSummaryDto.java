package com.example.blogZenikaeval.blog.web.articles;

public class ArticleSummaryDto {

    private String id;
    private String title;
    private String author;
    private String summary;


    public ArticleSummaryDto(String id, String title,String author, String summary) {
        this.title = title;
        this.author = author;
        this.summary = summary;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }
}


