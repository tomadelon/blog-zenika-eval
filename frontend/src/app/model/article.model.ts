export interface Article {
    id: string,
    title: string,
    author: string,
    summary: string,
    content: string,
}