import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';


@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit {

  public id: any = '';
  public title: string = '';
  public summary: string = '';
  public content: string = '';

  constructor(private activatedRoute: ActivatedRoute, private articleService: ArticleService, private route: Router) { }

  onSubmitEditArticle(): void {
    this.articleService.editArticle(this.id, this.title, this.summary, this.content),
      this.route.navigate(["/"]);
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.id=params['id']
      this.articleService.getArticleDetail(this.id);
  })
  
  }
}
