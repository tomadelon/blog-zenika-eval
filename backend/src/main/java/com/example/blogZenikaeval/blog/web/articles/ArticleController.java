package com.example.blogZenikaeval.blog.web.articles;

import com.example.blogZenikaeval.blog.application.ArticleService;
import com.example.blogZenikaeval.blog.domain.model.article.Article;
import com.example.blogZenikaeval.blog.domain.model.article.BadLengthSummaryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleService articleService;

    @Autowired
    public ArticleController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Article createArticle(@RequestBody ArticleDto articleDto) throws BadLengthSummaryException {
        SecurityContextHolder.getContext().getAuthentication().getName();
        return this.articleService.createArticle(articleDto.getTitle(), articleDto.getAuthor(), articleDto.getSummary(), articleDto.getContent());
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public Iterable<ArticleSummaryDto> fetchAllArticles(@RequestParam(name = "title", required = false) String title) {
        Iterable<Article> allArticles = Collections.emptyList();
        if (title != null) {
            allArticles = this.articleService.getAllArticleByTitle(title);
        } else {
            allArticles = this.articleService.getAllArticles();
        }
            List<ArticleSummaryDto> articlesDtoFor = new ArrayList();
            for (Article article : allArticles){
                articlesDtoFor.add(new ArticleSummaryDto(article.getId(), article.getTitle(), article.getAuthor(), article.getSummary()));
        }
        return articlesDtoFor;
    }

    @GetMapping("/{articleId}")
    public Optional<Article>
    findArticleById(@PathVariable("articleId") String articleId) {
        return this.articleService.getArticle(articleId);
    }

    @PutMapping("/{articleId}")
    ResponseEntity<Article> updateArticle(@PathVariable String articleId, @RequestBody UpdateArticleDto body) {
        Article updatedArticle = articleService.updateArticle(articleId, body.title(), body.summary(), body.content());
        return ResponseEntity.status(HttpStatus.OK).body(updatedArticle);
    }

    @DeleteMapping("/{articleId}")
    @ResponseStatus(HttpStatus.OK)
    void deleteArticle(@PathVariable String articleId) {
        articleService.deleteArticle(articleId);
    }

    /** @GetMapping("/{articleId}") public ResponseEntity <Article> getOneArticleById(@PathVariable("articleId") String articleId) {
    Optional <Article> foundArticle = this.articleManager.getArticle(articleId);
    return foundArticle.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }*/


}

