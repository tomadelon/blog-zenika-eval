import { Injectable } from '@angular/core';
import axios from 'axios';
import { Article } from '../model/article.model';
import { User } from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
 public user?: User; 

 apiUrl: String = 'http://localhost:8080/users';
  constructor() { }

  public createUser(email: string, username:string, password:string): Promise<User> {
    return axios.post(`${this.apiUrl}`, {
      email: email,
      username: username,
      password: password
    })
    .then(function (response) {
      console.log(response.data);
      return response.data
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

}
