import { Component, Input, OnInit } from '@angular/core';
import { Article } from 'src/app/model/article.model';
import { ArticleService } from 'src/app/services/article.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  
  @Input() article?: Article;

  constructor() { }
  
  ngOnInit(): void {

  }

}

