create extension if not exists pg_trgm;

create table if not exists users
(
    tid char(36) primary key,
    email text not null unique,
    username text not null unique,
    password text not null
);

create table if not exists articles
(
    id char(36) primary key,
    title text not null unique,
    author text not null,
    summary text not null,
    content text not null
);

insert into users values ('380eba2b-a920-4a11-b36b-8c613370609f', 'admin@admin.com', 'admin', '$2a$10$aJmcH58nAN5Tc7fQcmxwGudpxaP3EXZKERQHMOWq5mG2oRUGsvUDG') on conflict do nothing;