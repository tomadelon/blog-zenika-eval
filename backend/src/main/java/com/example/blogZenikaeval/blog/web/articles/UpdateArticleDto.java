package com.example.blogZenikaeval.blog.web.articles;

public record UpdateArticleDto(String title, String summary, String content) {
}


