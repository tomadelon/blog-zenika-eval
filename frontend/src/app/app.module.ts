import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginComponent } from './pages/login/login.component';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { ArticleComponent } from './components/article/article.component';
import { CreateArticleComponent } from './pages/create-article/create-article.component';
import { FormsModule } from '@angular/forms';
import { LucideAngularModule } from 'lucide-angular';
import { icons } from 'lucide-angular/';
import { EditArticleComponent } from './pages/edit-article/edit-article.component';
import { HeaderComponent } from './components/header/header.component';
import { CreateUserComponent } from './pages/create-user/create-user.component';



@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoginComponent,
    ArticleDetailComponent,
    ArticleComponent,
    CreateArticleComponent,
    EditArticleComponent,
    HeaderComponent,
    CreateUserComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    LucideAngularModule.pick({}),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
