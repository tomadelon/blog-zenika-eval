package com.example.blogZenikaeval.blog.domain.repository;

import com.example.blogZenikaeval.blog.domain.model.article.Article;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArticleRepository extends CrudRepository<Article,String> {
    @Query(value = "SELECT *\n" +
            "FROM articles\n" +
            "WHERE word_similarity(?1, articles.title) > 0.2",
            nativeQuery = true)

    public Iterable<Article> findByTitle(String title);
}
