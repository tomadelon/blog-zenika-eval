import { Component, OnInit, Output, EventEmitter,Inject } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ArticleService } from 'src/app/services/article.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HomePageComponent } from '../home-page/home-page.component';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss']
})
export class ArticleDetailComponent implements OnInit {

  constructor(public authentService: AuthenticationService, private activatedRoute: ActivatedRoute, public articleService: ArticleService, private route: Router) { }


  public clickOnDeleteButton() {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.deleteArticle(params['id']);
      this.route.navigate(['/']);

    })

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.articleService.getArticleDetail(params['id']);
    })

  }


}
