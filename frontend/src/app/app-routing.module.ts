import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleDetailComponent } from './pages/article-detail/article-detail.component';
import { CreateArticleComponent } from './pages/create-article/create-article.component';
import { CreateUserComponent } from './pages/create-user/create-user.component';
import { EditArticleComponent } from './pages/edit-article/edit-article.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  { path: '', component: HomePageComponent},
  { path: 'article', component: CreateArticleComponent},
  { path: 'article/:id', component: ArticleDetailComponent }, 
  { path: 'login', component: LoginComponent},
  { path: 'edit/:id', component: EditArticleComponent},
  { path: 'user', component: CreateUserComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
