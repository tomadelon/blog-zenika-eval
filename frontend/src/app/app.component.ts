import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { authInterceptor } from './helpers/interceptor';
import { Article } from './model/article.model';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'frontend';

  constructor(private authentService: AuthenticationService) {
    authInterceptor(this.authentService);
  }

}
