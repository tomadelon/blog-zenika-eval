import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import axios, { AxiosError, AxiosRequestConfig } from 'axios'
import { AppModule } from './app/app.module';
import { environment } from './environments/environment'
import { AuthenticationService } from './app/services/authentication.service';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
